package com.text.search;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;
import org.eclipse.collections.api.bag.MutableBag;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.collector.Collectors2;
import org.eclipse.collections.impl.list.mutable.FastList;

class Phrase {

  private String text;

  public Phrase(String text) {
    this.text = text;
  }


  public boolean equals(Object o) {
    return this.text.contains("" + o);
  }
}

public class Search {

  public static Map<String, List<String>> PHRASES = new HashMap<>();

  public static String inputText =
      "Afterwards, soon yellow bird landed on the tall tree in addition to a "
      + "lazy "
      + "tortoise.  "
      +
      "However, he had a read beak. In addition to white the patches on "
      + "the wings, "
      + "he was "
      + "completely yellow. "
      +
      "In summary, it was yellow bird. In summary, it did not sing.";

  static {
    PHRASES.put("1", Arrays
        .asList("afterward", "whenever", "however", "until", "as soon as",
                "as long as",
                "while",
                "again", "also", "besides"));
    PHRASES.put("2", Arrays
        .asList("therefore", "thus", "consequently", "as a result",
                "for this reason",
                "so",
                "so that", "accordingly", "because"));
    PHRASES.put("3",
                Arrays.asList("in addition to", "so", "moreover", "to", "the"));
    PHRASES.put("4", Arrays
        .asList("in general", "for the most part", "as a general rule",
                "on the whole",
                "usually",
                "typically", "in summary"));
  }

  public static Map<String, Integer> frequenciesTrieSearch(String[] phrasesArray,
                                                           String inputText) {
    MutableList<String> phrases = FastList.newListWith(phrasesArray)
                                          .collect(String::toLowerCase);
    PhraseEnabledTrie trie = new PhraseEnabledTrie();
    phrases.forEach(phrase -> trie.insert(phrase));

    // Scan for matches
    MutableBag<String> matches = TrieInputScanner
        .scanForMatches(trie, phrases, inputText);

    // Map original phrases to matches
    final MutableMap<String, Integer> matchesCount = phrases.stream().collect(
        Collectors2.toMap(
            each -> each,
            each -> matches.occurrencesOf(each)
        )
    );

    return matchesCount;
  }

  /**
   *
   * @param phrasesArray
   * @param inputText
   * @return
   */
  public static Map<String, Integer> frequenciesProblem1(String[] phrasesArray,
                                                         String inputText) {
    MutableList<String> phrases = FastList.newListWith(phrasesArray)
                                          .collect(each -> each.toLowerCase());
    Trie trieSearch = Trie.builder().onlyWholeWords().addKeywords(phrases)
                          .build();
    Collection<Emit> results = trieSearch.parseText(inputText.toLowerCase());

    MutableBag<String> matches = FastList.newList(results)
                                         .collect(Emit::getKeyword)
                                         .toBag();
    final MutableMap<String, Integer> matchesCount = phrases.stream().collect(
        Collectors2.toMap(
            each -> each,
            each -> matches.occurrencesOf(each)
        )
    );

    return matchesCount;
  }

  /**
   * Output the frequency in the format frequency:  "keyword": n, "keyword2":
   * n2, "keyword3": n3
   */
  public static String frequencyOutput(Map<String, Integer> freqToCount) {
    StringWriter sw = new StringWriter();

    sw.write("frequencies:  ");

    FastList.newList(freqToCount.keySet())
            .sortThis(String::compareTo)
            .forEach(phr -> sw
                .append("\"" + phr + "\": " + freqToCount.get(phr) + ", "));
    String output = sw.toString();

    return output.substring(0, output.length() - 2);

  }

  /**
   * Based on a set of phrases, and an input text, output a map of keyword ->
   * frequency.
   */
  public static Map<String, Integer> frequencyVector(MutableSet<String> phrases,
                                                     String inputText) {
    Trie trie = Trie.builder().ignoreCase().onlyWholeWords()
                    .addKeywords(phrases).build();
    Collection<Emit> emits = trie.parseText(inputText);
    MutableBag<String> matches = FastList.newList(emits)
                                         .collect(Emit::getKeyword)
                                         .toBag();

    final Map<String, Integer> map = phrases.stream()
                                            .collect(
                                                Collectors.toMap(
                                                    each -> each,
                                                    each -> matches.occurrencesOf(
                                                        each)
                                                )

                                            );
    return map;
  }

  /**
   * Output unique keyword set in format [ "keyword1", "keyword2" ... ] Output
   * counts in the format [ c1, c2, c3 ... ]
   */
  public static String frequencyVectorOutput(Map<String, Integer> freqToCount) {
    StringWriter sw = new StringWriter();
    ObjectWriter prettyPrinter = new ObjectMapper()
        .writerWithDefaultPrettyPrinter();

    String output = "";
    try {
      prettyPrinter.writeValue(sw, freqToCount.keySet().toArray());
      sw.append("\n");
      prettyPrinter.writeValue(sw, freqToCount.values().toArray());
      output = sw.toString();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return output;
  }

  /**
   * Return a list of phrases
   */
  public static MutableList<String> phrasesList() {
    return PHRASES.values()
                  .stream()
                  .flatMap(List::stream)

                  .collect(Collectors2.toList());

  }

  public static String[] phrasesArray() {
    return phrasesList()
        .toArray(new String[0]);


  }

  public static MutableSet<String> phrasesSet() {
    return PHRASES.values()
                  .stream()
                  .flatMap(List::stream)
                  .collect(Collectors2.toSet());

  }

  public static MutableList<Phrase> inputTextPhrases() {
    return FastList.newListWith(Pattern.compile("\\s*\\p{Punct}\\s*")
                                       .split(inputText))
                   .collect(in -> new Phrase(in.toLowerCase()));
  }

}
