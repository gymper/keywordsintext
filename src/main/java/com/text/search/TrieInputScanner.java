package com.text.search;

import org.eclipse.collections.api.bag.MutableBag;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.impl.bag.mutable.HashBag;
import org.eclipse.collections.impl.collector.Collectors2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Map;
import java.util.Scanner;

/**
 * Input scanner for PhraseEnabledTrie
 */
public class TrieInputScanner {

  private static final String  DELIM       = " ";
  private static final Pattern PUNCTUATION = Pattern.compile("[\\p{P}&&[^']]");

  private static Scanner newScanner(String inputText) {
    Scanner scanner = new Scanner(inputText);
    scanner.useDelimiter(DELIM);
    return scanner;
  }

  static class ScannerState {

    boolean inPhrase;
    boolean endPhrase;
    String  phrase;
    String  inputText;
    int     savePos;

    ScannerState(String text) {
      inPhrase = false;
      endPhrase = false;
      phrase = null;
      inputText = text;
      savePos = 0;
    }

    String addWordToPhrase(String word) {
      if (phrase == null)
        phrase = word;
      else
        phrase = phrase.concat(" " + word);

      return phrase;
    }

    void setStartInPhrase(String word, int save) {
      phrase = word;
      inPhrase = true;
      endPhrase |= false;
      savePos = save;
    }

    void endInPhrase() {
      inPhrase = false;
      endPhrase = false;
      phrase = null;
      savePos = 0;
//      Scanner scanner =
//          newScanner(inputText.substring(savePos, inputText.length() - 1));
//      return scanner;
    }
  }

  public static MutableBag<String> scanForMatches(PhraseEnabledTrie trie,
                                                  MutableList<String> phrases,
                                                  String inputText) {
    MutableBag<String> matches = HashBag.newBag();
    inputText = inputText.toLowerCase();
    Scanner      scanner = newScanner(inputText);
    ScannerState state   = new ScannerState(inputText);

    while (scanner.hasNext()) {
      String   word  = scanner.next();
      String[] words = PUNCTUATION.split(word);
      if (words.length == 1 && !words[0].equals(word)) {
        word = words[0];
        state.endPhrase = true;
      }
      PhraseEnabledTrie.TrieSearchResult searchResult;
      try {
        if (state.inPhrase) {
          String phrase = state.addWordToPhrase(word);
          searchResult = trie.search(phrase);
        } else
          searchResult = trie.search(word);
      } catch (IllegalArgumentException e) {
        throw new IllegalArgumentException(
            "Unable to parse input text because of word: "
            + word + " Error: " + e.getMessage());
      }

      switch (searchResult) {
        case PREFIX:
          // Is it not only part of a phrase, but also
          // a match on a prefix. Example "to boldly go" and "to"
          if (phrases.contains(word))
            matches.add(word);

          if (!state.inPhrase && !state.endPhrase)  // start of phrase
            state.setStartInPhrase(word, scanner.match().end());
          else if (phrases.contains(state.phrase))  // match partial phrases
            matches.add(state.phrase);

          break;

        case WHOLE_PHRASE:
          if (state.inPhrase) {
            matches.add(state.phrase);
            if(phrases.contains(word))
              matches.add(word);
            // Rewind scanner to check for words
//            scanner = state.endInPhrase();
            state.endInPhrase();
          } else {
            matches.add(word);
          }
          break;

        case NO_MATCH:
//          System.out.println("No match on " + word);
          if(state.inPhrase)
            state.endInPhrase();
          break;
        default:
          break;
      }
      // punctuation so end phrase
      if (state.inPhrase && state.endPhrase)
        state.endInPhrase();
      else
        state.endPhrase = false;

    }

    return matches;
  }
}
