package com.text.search;


/**
 * Initially was going to use a basic TrieSearch. It didn't seem as efficient
 * when I thought of actually using it, because it doesn't handle phrases well.
 * If I were splitting on words, this would work fine. Since I'm matching
 * phrases, I had to come up with another solution.
 */
public class PhraseEnabledTrie {

  // English alphabet plus space (for phrases) and quotation mark (don't)
  private static final int ALPHABET_LENGTH            = 28;
  private static final int SPACE_INDEX                = ALPHABET_LENGTH - 1;
  private static final int SINGLE_QUOTE_INDEX         = SPACE_INDEX - 1;
  private static final int ALPHABET_LAST_LETTER_INDEX = SINGLE_QUOTE_INDEX - 1;

  private TrieNode root;

  public PhraseEnabledTrie() {
    root = new TrieNode();
  }

  public void insert(String phrase) {
    TrieNode top = root;
    for (int i = 0; i < phrase.length(); i++) {
      int idx = alphabetIndex(phrase.charAt(i));
      if (top.nodes[idx] == null)
        top = top.nodes[idx] = new TrieNode();
      else
        top = top.nodes[idx];
      // Don't allow end phrase if another phrase is bigger
      if (top.isEndPhrase && i != phrase.length() - 1)
        top.isEndPhrase = false;
    }
    top.isEndPhrase = true;
  }

  /**
   * Allow for space
   */
  private int alphabetIndex(char ch) throws IllegalArgumentException {
    switch (ch) {
      case ' ':
        return SPACE_INDEX;
      case '\'':
        return SINGLE_QUOTE_INDEX;
      default:
        int idx = ch - 'a';
        if (idx < 0 || idx > ALPHABET_LAST_LETTER_INDEX)
          throw new IllegalArgumentException(ch + ": is not a valid input");
        return idx;
    }
  }

  public TrieSearchResult search(String phrase)
      throws IllegalArgumentException {
    TrieNode node = findNode(phrase);

    return node == null ?
           TrieSearchResult.NO_MATCH
                        : node.isEndPhrase
                          ? TrieSearchResult.WHOLE_PHRASE
                          : TrieSearchResult.PREFIX;
  }


  protected TrieNode findNode(String phrase) throws IllegalArgumentException {
    TrieNode top = root;
    for (int i = 0; i < phrase.length(); i++) {
      int idx;
      try {
        idx = alphabetIndex(phrase.charAt(i));
      } catch (IllegalArgumentException e) {
        throw new IllegalArgumentException(
            phrase + " contains characters I don't understand. "
            + e.getMessage());
      }
      if (top.nodes[idx] != null)
        top = top.nodes[idx];
      else
        return null;
    }
    if (top == root)
      return null;

    return top;
  }


  public enum TrieSearchResult {
    WHOLE_PHRASE,
    PREFIX,
    NO_MATCH
  }

  static class TrieNode {

    TrieNode[] nodes;
    boolean    isEndPhrase = false;

    public TrieNode() {
      nodes = new TrieNode[ALPHABET_LENGTH];
    }
  }
}
