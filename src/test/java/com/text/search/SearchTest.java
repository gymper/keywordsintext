package com.text.search;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.noconnor.junitperf.JUnitPerfRule;
import com.github.noconnor.junitperf.JUnitPerfTest;
import org.eclipse.collections.api.list.ImmutableList;
import org.eclipse.collections.api.map.ImmutableMap;
import org.eclipse.collections.api.set.ImmutableSet;
import org.eclipse.collections.api.set.MutableSet;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchTest {

  @Rule
  public JUnitPerfRule perfTestRule = new JUnitPerfRule();

  public static Map<String, List<String>> PHRASES = new HashMap();

  static {
    PHRASES.put("1",
                Arrays.asList("afterward",
                              "whenever",
                              "however",
                              "until",
                              "as soon as",
                              "as long as",
                              "while",
                              "again",
                              "also",
                              "besides"));
    PHRASES.put("2",
                Arrays.asList("therefore",
                              "thus",
                              "consequently",
                              "as a result",
                              "for this reason",
                              "so",
                              "so that",
                              "accordingly",
                              "because"));
    PHRASES.put("3", Arrays.asList("in addition to", "so", "moreover"));
    PHRASES.put("4",
                Arrays.asList("in general",
                              "for the most part",
                              "as a general rule",
                              "on the whole",
                              "usually",
                              "typically",
                              "in summary"));
  }

  public static String inputText =
      "Afterwards, soon yellow bird landed on the tall tree in addition to a "
      + "lazy tortoise.  "
      +
      "However, he had a read beak. In addition to white the patches on the "
      + "wings, he was completely yellow. "
      +
      "In summary, it was yellow bird. In summary, it did not sing.";

  @Test
  public void testProblem1Simple() throws JsonProcessingException {
    // From comments test
    String[] keywords = new String[]{"until", "so", "usually"};
    String input =
        "So, I have not seen this. So, usually, I don't follow all trends.";
    Map<String, Integer> counts =
        Search.frequenciesProblem1(keywords, input);
    String output = Search.frequencyOutput(counts);
    String keywordText =
        new ObjectMapper().writeValueAsString(keywords);
    System.out.println("Keywords: " + keywordText);
    System.out.println("Input: " + input + "\n");
    System.out.println(output);

    Assert.assertEquals("Outputs should be the same",
                        "frequencies:  \"so\": 2, \"until\": 0, \"usually\": 1",
                        output);
  }

  @Test
  @JUnitPerfTest(threads = 5, durationMs = 10_000, warmUpMs = 1_000, maxExecutionsPerSecond = 20)
  public void testProblem1() throws JsonProcessingException {

    // From data test
    String[] phrases = Search.phrasesArray();
    Map<String, Integer> counts2 =
        Search.frequenciesProblem1(phrases, inputText);
    String output2 = Search.frequencyOutput(counts2);
    String keywords =
        new ObjectMapper().writeValueAsString(phrases);
//    System.out.println("Keywords: " + keywords);
//    System.out.println("Input: " + inputText + "\n");
//
//    System.out.println(output2);
  }


  @Test
  public void testTriesNestedPhrase() throws JsonProcessingException {
    String[] keywords = new String[]{"have", "have not"};
    String input =
        "So, I have not seen this. So, usually, I don't follow all trends.";

    Map<String, Integer> counts =
        Search.frequenciesTrieSearch(keywords, input);
    String output = Search.frequencyOutput(counts);
    String keywordText =
        new ObjectMapper().writeValueAsString(keywords);

    System.out.println("Keywords: " + keywordText);
    System.out.println("Input: " + input + "\n");
    System.out.println(output);

    Assert.assertEquals("Outputs should be the same",
                        "frequencies:  \"have\": 1, \"have not\": 1",
                        output);

  }

  @Test
  public void testTriesContractions() throws JsonProcessingException {
    String[] keywords = new String[]{"don't"};
    String input =
        "So, I have not seen this. So, usually, I don't follow all trends.";

    Map<String, Integer> counts =
        Search.frequenciesTrieSearch(keywords, input);
    String output = Search.frequencyOutput(counts);
    String keywordText =
        new ObjectMapper().writeValueAsString(keywords);

    System.out.println("Keywords: " + keywordText);
    System.out.println("Input: " + input + "\n");
    System.out.println(output);

    Assert.assertEquals("Outputs should be the same",
                        "frequencies:  \"don't\": 1",
                        output);

  }


  @Test
  public void testTriesPhraseWhereInputHasPunctuation()
      throws JsonProcessingException {
    String[] keywords = new String[]{"so", "so usually"};
    String input =
        "So, I have not seen this. So, usually, I don't follow all trends.";

    Map<String, Integer> counts =
        Search.frequenciesTrieSearch(keywords, input);
    String output = Search.frequencyOutput(counts);
    String keywordText =
        new ObjectMapper().writeValueAsString(keywords);

    System.out.println("Keywords: " + keywordText);
    System.out.println("Input: " + input + "\n");
    System.out.println(output);

    Assert.assertEquals("Outputs should be the same",
                        "frequencies:  \"so\": 2, \"so usually\": 0",
                        output);

  }

  @Test
  public void testProblem1PhrasesTrie() throws JsonProcessingException {
    String[] keywords = new String[]{"until",
                                     "so",
                                     "usually",
                                     "so usually",
                                     "have",
                                     "have not",
                                     "don't"};
    String input =
        "So, I have not seen this. So, usually, I don't follow all trends.";

    Map<String, Integer> counts =
        Search.frequenciesTrieSearch(keywords, input);
    String output = Search.frequencyOutput(counts);
    String keywordText =
        new ObjectMapper().writeValueAsString(keywords);
    System.out.println("Keywords: " + keywordText);
    System.out.println("Input: " + input + "\n");
    System.out.println(output);

//        Assert.assertEquals("Outputs should be the same", "frequencies:
// \"so\": 2, \"until\": 0, \"usually\": 1", output);
  }

  @Test
  @JUnitPerfTest(threads = 5, durationMs = 10_000, warmUpMs = 1_000, maxExecutionsPerSecond = 20)
  public void testProblem2Tries() throws JsonProcessingException {
    MutableSet<String> phrasesSet = Search.phrasesSet();
    String[]           phrases    = phrasesSet.toArray(new String[0]);

    Map<String, Integer> counts =
        Search.frequenciesTrieSearch(phrases, inputText);
    String keywords =
        new ObjectMapper().writeValueAsString(phrases).replace("[]", "]");
    System.out.println("Keywords: " + keywords);
    System.out.println("Input: " + inputText + "\n");

    String output = Search.frequencyVectorOutput(counts);
    System.out.println(output);
  }

  @Test
  public void testProblem2() throws JsonProcessingException {
    MutableSet<String> phrasesSet = Search.phrasesSet();
    Map<String, Integer> counts =
        Search.frequencyVector(phrasesSet, inputText);
    String[] phrases = phrasesSet.toArray(new String[0]);
    String keywords =
        new ObjectMapper().writeValueAsString(phrases).replace("[]", "]");
    System.out.println("Keywords: " + keywords);
    System.out.println("Input: " + inputText + "\n");

    String output = Search.frequencyVectorOutput(counts);
    System.out.println(output);
  }

  @Test
  public void collectionsTest() throws JsonProcessingException {
    ImmutableSet<String> phrasesSet = Search.phrasesSet().toImmutable();
    ImmutableList<Phrase> inputPhrases =
        Search.inputTextPhrases().toImmutable();
    ImmutableMap<String, Long> agg = phrasesSet.aggregateBy(
        word -> word,
        () -> new Long(0),
        (sum, each) ->
            sum + inputPhrases
                .parallelStream().filter(in -> in.equals(each))
                .count()
    );
    ObjectWriter prettyPrinter =
        new ObjectMapper().writerWithDefaultPrettyPrinter();
    System.out.println(prettyPrinter.writeValueAsString(agg));

  }


}