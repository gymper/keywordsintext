# Searching Test

## Problem Domain


Description:  Please code the two problems as described below using Java 8 preferably (or Java 7 if necessary).  Feel free to create classes,
structures, interfaces, or any other construct needed to solve this.  Note: the keys (1,2,3,4) in PHRASES have no particular significace to this.
Problem 1) Get the frequencies of each phrase from PHRASES above, ie how many times the phrase appears in the text as described in inputText,
but it should be able to work with any text.
  
For example, if you have the following:
phrases =  ["until, "so", "usually"]
text = "So, I have not seen this. So, usually, I don't follow all trends."
  
It will create the following output:
frequencies:  "until":0, "so": 2, "usually":1


Problem 2) Based on the phrases in PHRASES, create a unique list of phrases (vocabulary) , then generate a vector/list that
represents the frequency of each phrase for the given text.

For example, if we have the following:
phrases = ["until, "so", "usually", "so"]
text = "So, I have not seen this. So, usually, I don't follow all trends."

It will have the following output:
vocabulary = ["so", "until", "usually"]
vector = [2, 0, 1]

Use the data structures below.


```java

	public static Map<String, List<String>> PHRASES = new HashMap<>();

	static {
		PHRASES.put("1", Arrays.asList("afterward", "whenever", "however", "until", "as soon as", "as long as", "while", "again", "also", "besides"));
		PHRASES.put("2", Arrays.asList("therefore", "thus", "consequently", "as a result", "for this reason", "so", "so that", "accordingly", "because"));
		PHRASES.put("3", Arrays.asList("in addition to", "so", "moreover"));
		PHRASES.put("4", Arrays.asList("in general", "for the most part", "as a general rule", "on the whole", "usually", "typically", "in summary"));
	}

	public static String inputText = "Afterwards, soon yellow bird landed on the tall tree in addition to a lazy tortoise.  " +
			"However, he had a read beak. In addition to white the patches on the wings, he was completely yellow. " +
			"In summary, it was yellow bird. In summary, it did not sing.";

```

## Description

This is the implementation of the search problems. I originally implemented
using Aho-Corasick with a library, as well as with Eclipse Collections,
but now just quickly implemented a Trie that enables phrases (PhraseEnabledTrie). 


## TRIES search to accept Phrases

I refactored tries search to accept phrases, and apostrophes.
I doesn't work for nested phrases - i.e "in addition" and
"in addition to", but it does work for keywords embedded in phrases -
"to" and "in addition to". 
There is logic that a phrase should not
match when the input text contains that phrase with punctuation.
I.e. - "so usually" should not match "so, usually". However, it should
match in this case "in addition" -> "in addition, and also".
It also works for contractions. I haven't fully tested all cases.


## Aho-Corasick - most efficient

I originally decided on the Aho-Corasick algorithm, which handles phrases better,
because it maintains state, handles nested keywords, and it has the benefit of being O(n) regardless
of the number of keywords or size of the text to search 
against.
https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm

I didn't implement this algorithm due to time constraints - I used an existing library (https://github.com/robert-bor/aho-corasick)
and tested it.

### Aho library - Partial words doesn't work as I would like

I would need to customize the library to allow partial matches only
if they are at the beginning of the string. If I choose
the `onlyWholeWords` customization, it will not match
"afterward" in "afterwards". which might be a reasonable
option to match.

## Tests

`./gradlew test`

Tests are in src/test/java/ com.text.search.SearchTest

### Aho-Corasick Tests

### testProblem1Simple()

The simple test from the Test.java:

        For example, if you have the following:
        phrases =  ["until, "so", "usually"]
        text = "So, I have not seen this. So, usually, I don't follow all trends."

  
  
### testProblem1() - performance tested

The test for frequency problem 1. See [Performance/Benchmarking](#performancebenchmark)

```java
@JUnitPerfTest(threads = 5, durationMs = 10_000, warmUpMs = 1_000, maxExecutionsPerSecond = 20)
```

### testProblem2()

The test for set of keywords and vector of keywords and
frequency.

### Eclipse Collections Test

### collectionsTest()

This is an eclipse collections implementation of the
problem 2, which splits the input text by punctuation,
and uses a parallel stream to count keyword matches.

Note: the solution is in the test.

I tested the performance on this - and it is half as good as
the PhrasesTries but degrading steeply. I also assume it would
degrade even more on larger texts.

### PhrasesEnabledTrie tests

### testTriesNestedPhrase()

Tests for "have" and "have not".

### testTriesContractions()

Test for "don't"


### testTriesPhraseWhereInputHasPunctuation()

Test that "so usually" keyword should not match "so, usually"

### testProblem1PhrasesTrie()

Test like the simple test above on multiple keywords.

### testProblem2Tries() - performance tested

Fixed infinite loop so re-enabled test. Performance tested with:

```java
@JUnitPerfTest(threads = 5, durationMs = 10_000, warmUpMs = 1_000, maxExecutionsPerSecond = 20)
```


## Performance/Benchmark


JUnitPerf on testProblem1 and testProblem2Tries - results in build/reports/junitperf_report.html

Note that testProblem1 using Aho-Corasick has better performance. I wrote
the PhrasesEnabledTrie quickly though and haven't optimized.




 







